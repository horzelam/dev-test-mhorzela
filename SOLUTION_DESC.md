# About the tests.

Project contains unit tests + IT tests.  
IT tests are by default skipped even in "install" phase as one of the test produces big input tmp file.  
To run these tests during integration-test phase "IT" profile must be used - e.g.:  
```
mvn clean install -PIT
```


# Note about run script - service.sh
Here I had to change line with :
```
sh -c "$CMD" > "$PIDFILE"
```
to 
```
bash -c "$CMD" > "$PIDFILE"
```
The former version on my mashine was not producing proper PID to file, however service was started.
#!/bin/bash
mvn clean package

# INFO about build (M.Horzela)
# to install without IT tests:
# mvn clean install
# to install with running IT tests:
# mvn clean install -PIT
package com.horzelam.busservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.horzelam.busservice.loader.BusRoutesLoader;
import com.horzelam.busservice.model.BusRoute;
import com.horzelam.busservice.resource.BusServiceResource;
import com.horzelam.busservice.service.BusService;
import org.apache.commons.lang3.Validate;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

/**
 * Main App which starts service on Jetty.
 */
public class App {

    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(App.class);
    private static final int PORT = 8088;
    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://localhost:"+PORT;

    private static App app;
    private final Path dataFilePath;
    private HttpServer grizzlyServer;

    /**
     * Main method to run App.
     *
     * @param args main args
     *
     * @throws Exception when unable to start the app
     */
    public static void main(String[] args) throws Exception {
        app = new App(args);
        app.run(PORT);
    }

    /**
     * Constructor.
     *
     * @param args app args
     */
    public App(final String[] args) {
        Validate.isTrue(args.length == 1, "Improper Service args amount: " + args.length);
        dataFilePath = Paths.get(args[0]);
        Validate.isTrue(Files.isReadable(dataFilePath), "Unable to read file: " + dataFilePath);
    }

    /**
     * Starts the News App Rest service
     *
     * @param port TCP port to expose REST services
     *
     * @throws Exception when failed to start service
     */
    public void run(final int port) throws Exception {

        final BusRoute[] data = new BusRoutesLoader().load(dataFilePath);
        final BusService service = new BusService(data);

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        // create JsonProvider to provide custom ObjectMapper
        JacksonJaxbJsonProvider provider = new JacksonJaxbJsonProvider();
        provider.setMapper(mapper);

        ResourceConfig resConfig = new ResourceConfig();
        resConfig.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(service).to(BusService.class);
            }
        });

        resConfig.register(BusServiceResource.class);
        resConfig.register(provider);

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        grizzlyServer = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), resConfig);


        LOGGER.info("BusService REST is running on: " + port);

    }

    public void stop() throws Exception {
        if(grizzlyServer != null && grizzlyServer.isStarted()) {
            LOGGER.info("Stopping BusService REST service ...");
            grizzlyServer.shutdownNow();
        }
    }
}

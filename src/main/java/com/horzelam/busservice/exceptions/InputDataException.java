package com.horzelam.busservice.exceptions;

import java.io.IOException;

/**
 * When unable to laod the input bus routes data.
 */
public class InputDataException extends RuntimeException {
    private static final long serialVersionUID = 2327440150983224229L;

    /**
     * Constructor
     *
     * @param message exception message
     * @param cause   cause exception
     */
    public InputDataException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

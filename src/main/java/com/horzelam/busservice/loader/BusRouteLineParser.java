package com.horzelam.busservice.loader;

import com.horzelam.busservice.model.BusRoute;
import org.apache.commons.lang3.Validate;

/**
 * Parses single line with bus route data.
 */
public class BusRouteLineParser {
    private static final String DATA_SEPARATOR = " ";
    private final int maxStationsPerRoute;

    /**
     * Constructor.
     * @param maxStationsPerRoute maximum expected stations per route
     */
    public BusRouteLineParser(final int maxStationsPerRoute) {
        this.maxStationsPerRoute = maxStationsPerRoute;
    }

    /**
     * Parses single line and produces BusRoute.
     *
     * @param line line to be parsed
     *
     * @throws IllegalArgumentException when unable to parse the line
     * @throws NumberFormatException    when unable to parse the identifers in line
     */
    public BusRoute parseLine(final String line) {
        final String[] lineSplit = line.split(DATA_SEPARATOR);

        // check range of data (route id + stations) in single line:
        Validate.isTrue((lineSplit.length >= 3) && (lineSplit.length <= maxStationsPerRoute+ 1),
                "File contains improper amount of data in line : %s", line);
        // get route Id
        final int routeId = Integer.parseInt(lineSplit[0]);

        // get stations Ids
        final int[] stationsIdsArray = new int[lineSplit.length - 1];
        for(int itemNr = 1; itemNr < lineSplit.length; itemNr++) {
            final String idAsString = lineSplit[itemNr];
            stationsIdsArray[itemNr - 1] = Integer.parseInt(idAsString);

        }
        return new BusRoute(routeId, stationsIdsArray);
    }
}

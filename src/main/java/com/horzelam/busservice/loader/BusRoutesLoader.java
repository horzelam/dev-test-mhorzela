package com.horzelam.busservice.loader;

import com.horzelam.busservice.exceptions.InputDataException;
import com.horzelam.busservice.model.BusRoute;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

/**
 * Responsible for loading bus routes from file to model.
 */
public class BusRoutesLoader {

    //these values could be configurable e.g. in properties/yaml file:
    static final int MAX_EXPECTED_ROUTES = 100000;
    static final int MAX_EXPECTED_STATIONS_IN_ROUTE = 1000;

    private static final Logger LOGGER = LoggerFactory.getLogger(BusRoutesLoader.class);

    private final BusRouteLineParser parser = new BusRouteLineParser(MAX_EXPECTED_STATIONS_IN_ROUTE);

    /**
     * Loads the data from given path to set of bus routes
     *
     * @param dataFilePath path to file with data
     *
     * @return bus routes
     *
     * @throws IOException when unable to read the file
     */
    public final BusRoute[] load(final Path dataFilePath) {
        LOGGER.info("Loading data from {}", dataFilePath);
        final BusRoute[] busRoutes;

        // read 1st line to validate the size first
        try(final Stream<String> lines = Files.lines(dataFilePath, Charset.forName("UTF-8"))) {
            final int expectedBusRoutes = Integer.parseInt(lines.findFirst().get());
            Validate.validState(expectedBusRoutes <= MAX_EXPECTED_ROUTES, "Improper input data size: " + expectedBusRoutes);
            busRoutes = new BusRoute[expectedBusRoutes];
            LOGGER.info("Bus routes to load: {} . Loading the data...", expectedBusRoutes);
        } catch(IOException e) {
            throw new InputDataException("Unable to load data from file: " + dataFilePath, e);
        }

        // 2nd read - all the rest
        AtomicInteger i = new AtomicInteger(0);
        try(final Stream<String> lines = Files.lines(dataFilePath, Charset.forName("UTF-8"))) {
            lines.sequential().skip(1).forEachOrdered(line -> {
                busRoutes[i.getAndIncrement()] = parser.parseLine(line);
            });
            LOGGER.info("Bus Routes data loaded successfully. Bus routes loaded: {}", busRoutes.length);
            return busRoutes;
        } catch(IOException | IllegalArgumentException e) {
            throw new InputDataException("Unable to load data from file: " + dataFilePath, e);
        }
    }

}
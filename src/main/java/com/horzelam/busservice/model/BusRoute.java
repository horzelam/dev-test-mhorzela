package com.horzelam.busservice.model;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Represents single bus route.
 */
public class BusRoute {

    private final int id;
    private final int[] stationsIds;

    /**
     * Constructor
     *
     * @param id          identifier of bus route
     * @param stationsIds array of stations identifiers
     */
    public BusRoute(final int id, final int[] stationsIds) {
        Validate.isTrue(stationsIds != null && stationsIds.length > 1, "Bus route should have at least 2 stations");
        this.id = id;
        this.stationsIds = stationsIds;
    }

    /**
     * Returns the bus route id
     *
     * @return the bus route id
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the stations ids
     *
     * @return the stations ids
     */
    public int[] getStationsIds() {
        return stationsIds;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}

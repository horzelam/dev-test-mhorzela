package com.horzelam.busservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents BusService Response.
 */
public class BusRouteResponse {

    private int depSid;

    private int arrSid;

    private boolean directBusRoute;

    /**
     * Constructor
     *
     * @param depSid         departure station Id
     * @param arrSid         arrival station Id
     * @param directBusRoute TRUE means that there is direct route
     */
    public BusRouteResponse(final int depSid, final int arrSid, final boolean directBusRoute) {
        this.depSid = depSid;
        this.arrSid = arrSid;
        this.directBusRoute = directBusRoute;
    }

    @JsonProperty("dep_sid")
    public int getDepSid() {
        return depSid;
    }

    @JsonProperty("arr_sid")
    public int getArrSid() {
        return arrSid;
    }

    @JsonProperty("direct_bus_route")
    public boolean isDirectBusRoute() {
        return directBusRoute;
    }
}

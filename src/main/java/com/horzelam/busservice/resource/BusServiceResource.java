package com.horzelam.busservice.resource;

import com.horzelam.busservice.model.BusRouteResponse;
import com.horzelam.busservice.service.BusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.HttpURLConnection;

/**
 * REST service for bus service
 */
@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
public class BusServiceResource {

    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(BusServiceResource.class);

    @Inject
    private BusService busService;

    /**
     * Returns article for a given topic
     *
     * @param depSidAsString departure sid
     * @param arrSidAsString arrival sid
     *
     * @return HTTP OK with json response or HTTP BAD REQUEST if improper params
     */
    @GET
    @Path("direct")
    @Produces(MediaType.APPLICATION_JSON)
    public BusRouteResponse get(@QueryParam("dep_sid") String depSidAsString, @QueryParam("arr_sid") String arrSidAsString) {
        try {
            final int depSid = Integer.parseInt(depSidAsString);
            final int arrSid = Integer.parseInt(arrSidAsString);

            LOGGER.info("Request :" + depSid + ", " + arrSid);

            final boolean directRouteExists = busService.doesDirectRouteExist(depSid, arrSid);

            return new BusRouteResponse(depSid, arrSid, directRouteExists);

        } catch(final NumberFormatException e) {
            throw new WebApplicationException(
                    Response.status(HttpURLConnection.HTTP_BAD_REQUEST).entity("Improper request parameters").build());
        }

    }
}

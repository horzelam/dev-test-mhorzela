package com.horzelam.busservice.service;

import com.horzelam.busservice.model.BusRoute;

import java.util.Arrays;
import java.util.Set;

/**
 * Bus service.
 */
public class BusService {

    private final BusRoute[] busRoutes;

    /**
     * Starts the service with given bus route data
     *
     * @param busRoutes bus route data
     */
    public BusService(final BusRoute[] busRoutes) {
        this.busRoutes = busRoutes;
    }

    /**
     * Returns information about existence of bus route between two stations
     *
     * @param depId departure station
     * @param arrId arrival station
     *
     * @return TRUE if route exists
     */
    public boolean doesDirectRouteExist(final int depId, final int arrId) {

        // Find any bus route
        // which has station ids matching depId and arrId
        return Arrays.stream(this.busRoutes).filter(busRoute -> Arrays.stream(busRoute.getStationsIds())
                // here we rely on fact that depId/arrId occurs only once in busRoute's station - because its a Set
                .filter(id -> id == depId || id == arrId)
                // and its enough to find just 2 first elements - no need to move further in the sub-stream
                .limit(2).count() == 2).findAny().isPresent();

    }
}

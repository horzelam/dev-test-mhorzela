package com.horzelam.busservice.it;

import com.horzelam.busservice.App;
import io.restassured.response.Response;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.nio.file.Paths;

import static io.restassured.RestAssured.get;
import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.equalTo;

/**
 * IT test for REST Resources.
 * It starts the server locally.
 */
public class AppRestIT {

    private static final int PORT = 8088;
    public static final String SERVICE_URL = "http://localhost:" + PORT + "/api/direct";
    private static App app;

    @BeforeClass
    public static void startTestedServiceLocally() throws Exception {
        final String filePath = Paths.get(AppRestIT.class.getResource("/bus_routes.txt").toURI()).toString();
        app = new App(new String[]{filePath});
        app.run(PORT);

    }

    @AfterClass
    public static void stopService() throws Exception {
        app.stop();
    }

    @Test
    public void shouldReturnPropperRespons(){
        // GIVEN request
        final String request = SERVICE_URL + "?dep_sid=3&arr_sid=6";

        // WHEN we run it
        final Response resp = get(request);

        // THEN response should
        resp.then()
                // have HTTP status 200 and it must be JSON with proper content:
                .statusCode(SC_OK)
                .and().contentType("application/json")
                .and().body("dep_sid", equalTo(3))
                .and().body("arr_sid", equalTo(6))
                .and().body("direct_bus_route", equalTo(true));
    }

    @Test
    public void shouldReturnHTTP400ForNonNumericArgs() {
        // GIVEN request
        final String request = SERVICE_URL + "?dep_sid=3&arr_sid=AA";

        // WHEN we run it
        final Response resp = get(request);

        // THEN response should
        resp.then()
                // have status BAD REQUEST
                .and().statusCode(SC_BAD_REQUEST);

    }

    @Test
    public void shouldReturnHTTP400ForEmptyArgs() {
        // GIVEN request
        final String request = SERVICE_URL;

        // WHEN we run it
        final Response resp = get(request);

        // THEN response should
        resp.then()
                // have status BAD REQUEST
                .and().statusCode(SC_BAD_REQUEST);

    }

}

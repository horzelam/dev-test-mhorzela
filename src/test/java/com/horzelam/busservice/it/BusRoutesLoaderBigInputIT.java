package com.horzelam.busservice.it;

import com.horzelam.busservice.loader.BusRoutesLoader;
import com.horzelam.busservice.model.BusRoute;
import com.horzelam.busservice.service.BusService;
import groovy.json.internal.Charsets;
import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test to check how the Loader works for extreme input data.
 * Temp File size produced in this test :  1000000 routes x 1000 stations -> 467 MB file in plain text.
 */
public class BusRoutesLoaderBigInputIT {
    private static final Logger LOGGER = LoggerFactory.getLogger(BusRoutesLoaderBigInputIT.class);
    private final int busRoutes = 100_000;
    private Path tmpDataFilePath;

    @BeforeMethod
    public void prepare() throws IOException {
        tmpDataFilePath = prepareTempDataFile(busRoutes, 1000);
    }

    @AfterMethod
    public void cleanup() {
        tmpDataFilePath.toFile().delete();
    }

    @Test(timeOut = 10_000)
    public void loadTest() throws URISyntaxException, IOException {
        final BusRoutesLoader loader = new BusRoutesLoader();

        // WHEN
        final BusRoute[] data = loader.load(tmpDataFilePath);

        // THEN
        assertThat(data).hasSize(busRoutes);
        // with additional random queries - increase the timeout limit a bit
        // doRandomQueries(data);
    }

    private void doRandomQueries(final BusRoute[] data) {
        final BusService serv = new BusService(data);
        for(int i = 0; i < 50; i++) {
            int sidDep = 10 * i + RandomUtils.nextInt(0, 9);
            int sidArr = (10 + 1) * i + RandomUtils.nextInt(0, 9);
            LOGGER.info(sidDep + "," + sidArr + " -> " + serv.doesDirectRouteExist(sidDep, sidArr));
        }
    }

    private Path prepareTempDataFile(final int maxLines, final int maxStations) throws IOException {
        final Path tmpFile = Files.createTempFile("bus_route_test_", null);

        final List<CharSequence> val = new ArrayList<>();
        val.add(String.valueOf(maxLines));

        for(int routeNr = 0; routeNr < maxLines; routeNr++) {
            StringBuilder sids = new StringBuilder();
            for(int sidNr = 0; sidNr < maxStations; sidNr++) {
                final int randomSid = RandomUtils.nextInt(sidNr * 10, (sidNr + 1) * 10);
                sids.append(" " + randomSid);
            }
            val.add(routeNr + sids.toString());
            if(routeNr % 10000 == 0) {
                LOGGER.info("Generating BusRoute nr:" + routeNr);
            }
        }
        Files.write(tmpFile, val, Charsets.UTF_8, StandardOpenOption.WRITE);
        LOGGER.info("Test file prepared: " + tmpFile.toString());
        return tmpFile;
    }

}

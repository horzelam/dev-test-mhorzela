package com.horzelam.busservice.loader;

import com.horzelam.busservice.model.BusRoute;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.assertThrows;
import static org.testng.Assert.expectThrows;

/**
 * Test for {@link BusRouteLineParserTest}
 */
public class BusRouteLineParserTest {

    @Test
    public void shouldParseSuccessfully() {
        // GIVEN
        final String line = "1 23 24 25";
        final BusRouteLineParser tested = new BusRouteLineParser(100);

        // WHEN
        final BusRoute result = tested.parseLine(line);

        // THEN
        assertThat(result).isEqualToComparingFieldByField(new BusRoute(1, new int[]{23, 24, 25}));

    }

    @Test
    public void shouldFailToParseDataBeyondLimit() {
        // GIVEN limit set to 2 and line with 3 stations Ids
        final String line = "1 23 24 25";
        final BusRouteLineParser tested = new BusRouteLineParser(2);

        // WHEN
        final IllegalArgumentException ex = expectThrows(IllegalArgumentException.class, () -> tested.parseLine(line));

        // THEN
        assertThat(ex).hasMessage("File contains improper amount of data in line : 1 23 24 25");

    }
}

package com.horzelam.busservice.loader;

import com.horzelam.busservice.model.BusRoute;
import org.testng.annotations.Test;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test for {@link BusRoutesLoader}
 */
public class BusRoutesLoaderTest {

    @Test
    public void shouldLoadDataWithSuccess() throws URISyntaxException {
        // GIVEN
        final Path path = Paths.get(getClass().getResource("/bus_routes.txt").toURI());
        final BusRoutesLoader loader = new BusRoutesLoader();

        // WHEN
        final BusRoute[] data = loader.load(path);

        // THEN
        assertThat(data).hasSize(3);
        assertThat(data[0]).isEqualToComparingFieldByField(new BusRoute(0, new int[]{0, 1, 2, 3, 4}));
        assertThat(data[1]).isEqualToComparingFieldByField(new BusRoute(1, new int[]{3, 1, 6, 5}));
        assertThat(data[2]).isEqualToComparingFieldByField(new BusRoute(2, new int[]{0, 6, 4}));

    }

}

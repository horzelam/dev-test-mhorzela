package com.horzelam.busservice.service;

import com.horzelam.busservice.model.BusRoute;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test for bus service.
 */
public class BusServiceTest {

    private static BusRoute[] busRoutes;

    @BeforeClass
    public static void prepareData() {
        busRoutes = new BusRoute[]{
                // few example routes:
                new BusRoute(1, new int[]{1, 2, 3}),
                new BusRoute(2, new int[]{1, 6, 7}),
                new BusRoute(3, new int[]{8, 9, 1})
        };
    }

    // routes which should exists:
    @DataProvider
    Object[][] expectedExistingRoutes() {
        return new Object[][]{{1, 3}, {1, 2}, {7, 6}, {1, 9}, {9, 8}};
    }

    // routes non-existing routes
    @DataProvider
    Object[][] expectedNonExistingRoutes() {
        return new Object[][]{{2, 6}, {2, 9}};
    }

    @Test(dataProvider = "expectedExistingRoutes")
    public void shouldFindTheRoute(final int depSidInput, final int arrSidInput) {
        // GIVEN
        final BusService tested = new BusService(busRoutes);

        // WHEN
        final boolean result = tested.doesDirectRouteExist(depSidInput, arrSidInput);

        // THEN
        assertThat(result).isTrue();
    }

    @Test(dataProvider = "expectedNonExistingRoutes")
    public void shouldNotFindTheRoute(final int depSidInput, final int arrSidInput) {
        // GIVEN
        final BusService tested = new BusService(busRoutes);

        // WHEN
        final boolean result = tested.doesDirectRouteExist(depSidInput, arrSidInput);

        // THEN
        assertThat(result).isFalse();
    }
}
